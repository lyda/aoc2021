#! /usr/bin/env python3

import fileinput
import collections

def main():
  timers = None
  for line in fileinput.input():
    line = line.rstrip()
    timers = collections.Counter([int(n) for n in line.split(',')])
  for day in range(80):
    for time_left in range(9):
      timers[time_left - 1] = timers.get(time_left, 0)
    timers[8] = timers.get(-1, 0)
    timers[6] = timers.get(6, 0) + timers.get(-1, 0)
    del timers[-1]
    #print('{0}: {1}'.format(day + 1, sum(timers.values())))
  print(sum(timers.values()))

if __name__ == '__main__':
  main()
