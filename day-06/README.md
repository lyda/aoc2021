# Day 6 - Lanternfish

## Problem - part 1

How do Lanternfish populations grow?

For part 1, the tricky bit was deciding which way it was going to go
in part 2.  The "exponential growth" in the problem description and
the limited example output made me think it would be unwise to track
individuals.

So part 2 could be more days, a more complex reproduction system or
modeling lifespans.  A population list seemed like the way to go.
I figured I could create generation lists for those populations if it
was the latter scenario.

  * [Problem](https://adventofcode.com/2021/day/6)
  * Source for input file:
    * `input/input` - `https://adventofcode.com/2021/day/6/input`

## Solution - part 1

  * Easy solve in python: `python/part1.py`
  * Redid it later in C: `c/main1.c`

## Problem - part 2

Went from calculation the population at 80 days to doing the same for
256 days.  That's simple.

  * Original problem: https://adventofcode.com/2021/day/6#part2
  * Source for input file (same data):
    * `input/input` - `https://adventofcode.com/2021/day/6/input`

## Solution - part 2

  * Easy in python: `python/part2.py`
  * Then did a C version: `c/main1.c` Make the number of days a `#define`
    so it's just the compile args that differentiate parts 1 and 2.
    Also, `uint64_t` is my friend.

On reddit loads of people tried to track individuals and it went badly.
But I learned a project goal was for solutions to finish in less than
a minute even on decade old hardware.  That would be useful on the 7th.
