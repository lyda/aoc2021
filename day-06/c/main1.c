#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>

#ifndef DAYS
#define DAYS 80
#endif

int
main(int argc, char *argv[]) {
  FILE *f;
  int c;
  uint64_t timers[9];

  for (int i = 0; i < 9; i++)
    timers[i] = 0;
  if (!(f = fopen(argv[1], "r"))) {
    perror("FATAL: Failed to open input file");
    exit(1);
  }
  while ((c = fgetc(f)) != EOF) {
    if (c == '\n')
      break;
    if (c == ',')
      continue;
    assert(c >= '0' && c < '9');
    timers[c - '0']++;
  }
  fclose(f);

  for (int i = 0; i < DAYS; i++) {
    uint64_t spawn = timers[0];
    for (int t = 0; t < 9 - 1; t++)
      timers[t] = timers[t + 1];
    timers[6] += spawn;
    timers[8] = spawn;
  }

  uint64_t population = 0;
  for (int t = 0; t < 9; t++)
    population += timers[t];
  printf("%" PRIu64 "\n", population);
  exit(0);
}
