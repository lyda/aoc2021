#!/bin/bash

read -rp "Day: " day
read -rp "Part: " part
read -rp "Answer: " answer
htpasswd -b -B -C 16 answers \
              "$(printf "day-%02d-%s" "$day" "$part")" "$answer"
