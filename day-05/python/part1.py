#! /usr/bin/env python3

import fileinput
import re

class Bingo(Exception):
  pass

class Line:
  def __init__(self, line):
    pattern = re.compile(r'(?P<px1>\d+),(?P<py1>\d+) -> (?P<px2>\d+),(?P<py2>\d+)')
    match = pattern.match(line)
    self.px1 = int(match.group('px1'))
    self.py1 = int(match.group('py1'))
    self.px2 = int(match.group('px2'))
    self.py2 = int(match.group('py2'))

  def horizontal(self):
    if self.px1 == self.px2:
      return True
    return False

  def vertical(self):
    if self.py1 == self.py2:
      return True
    return False

class Diagram:
  def __init__(self):
    self.diagram = dict()

  def add_line(self, line):
    if line.horizontal():
      py1 = min(line.py1, line.py2)
      py2 = max(line.py1, line.py2)
      for y in range(py1, py2 + 1):
        point = '{0},{1}'.format(line.px1, y)
        self.diagram[point] = self.diagram.get(point, 0) + 1
    elif line.vertical():
      px1 = min(line.px1, line.px2)
      px2 = max(line.px1, line.px2)
      for x in range(px1, px2 + 1):
        point = '{0},{1}'.format(x, line.py1)
        self.diagram[point] = self.diagram.get(point, 0) + 1

  def check_overlap(self):
    overlap = 0
    for _, times in self.diagram.items():
      if times > 1:
        overlap += 1
    return overlap

def main():
  diagram = Diagram()
  for line in fileinput.input():
    line = Line(line.rstrip())
    diagram.add_line(line)

  print(diagram.check_overlap())

if __name__ == '__main__':
  main()
