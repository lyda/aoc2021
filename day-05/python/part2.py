#! /usr/bin/env python3

import fileinput
import re

class Bingo(Exception):
  pass

class LineError(Exception):
  pass

class Line:
  def __init__(self, line):
    pattern = re.compile(r'(?P<px1>\d+),(?P<py1>\d+) -> (?P<px2>\d+),(?P<py2>\d+)')
    match = pattern.match(line)
    self.px1 = int(match.group('px1'))
    self.py1 = int(match.group('py1'))
    self.px2 = int(match.group('px2'))
    self.py2 = int(match.group('py2'))
    if self.px1 == max(self.px1, self.px2):
      self.px1, self.px2 = self.px2, self.px1
      self.py1, self.py2 = self.py2, self.py1

  def horizontal(self):
    if self.px1 == self.px2:
      return True
    return False

  def vertical(self):
    if self.py1 == self.py2:
      return True
    return False

  def __str__(self):
    return '{0},{1} -> {2},{3}'.format(self.px1, self.py1, self.px2, self.py2)

class Diagram:
  def __init__(self):
    self.diagram = dict()
    self.maxx = 0
    self.maxy = 0

  def __str__(self):
    diagram = ''
    y = 0
    while y <= self.maxy:
      diagram += ''.join([times for times in
                          [self.get_times(x, y) for x in range(self.maxx + 1)]]) + '\n'
      y += 1
    return diagram

  def add_point(self, x, y):
    point = '{0},{1}'.format(x, y)
    self.diagram[point] = self.diagram.get(point, 0) + 1

  def get_times(self, x, y):
    point = '{0},{1}'.format(x, y)
    times = self.diagram.get(point, 0)
    if times:
      return '{:0>1d}'.format(times)
    return '.'

  def add_line(self, line):
    self.maxx = max(self.maxx, line.px1, line.px2)
    self.maxy = max(self.maxy, line.py1, line.py2)
    if line.horizontal():
      py1 = min(line.py1, line.py2)
      py2 = max(line.py1, line.py2)
      for y in range(py1, py2 + 1):
        self.add_point(line.px1, y)
    elif line.vertical():
      for x in range(line.px1, line.px2 + 1):
        self.add_point(x, line.py1)
    elif line.py1 == min(line.py1, line.py2):
      x = line.px1
      y = line.py1
      while x <= line.px2:
        self.add_point(x, y)
        x += 1
        y += 1
      if y - 1 != line.py2:
        raise LineError(line)
    else:
      x = line.px1
      y = line.py1
      while x <= line.px2:
        self.add_point(x, y)
        x += 1
        y -= 1
      if y + 1 != line.py2:
        raise LineError(line)

  def check_overlap(self):
    overlap = 0
    for _, times in self.diagram.items():
      if times > 1:
        overlap += 1
    return overlap

def main():
  diagram = Diagram()
  for line in fileinput.input():
    line = Line(line.rstrip())
    diagram.add_line(line)

  print(diagram.check_overlap())

if __name__ == '__main__':
  main()
