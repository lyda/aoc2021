#include <assert.h>
#include <search.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct point {
  int x;
  int y;
  int times;
} point_t;

void *
point_key(int x, int y) {
  point_t *key;

  key = (point_t *)malloc(sizeof(point_t));
  key->x = x;
  key->y = y;
  key->times = 1;
  return (void *)key;
}

int
point_cmp(point_t *a, point_t *b) {
  if (a->x < b->x)
    return -1;
  if (a->x > b->x)
    return 1;
  if (a->y < b->y)
    return -1;
  if (a->y > b->y)
    return 1;
  return 0;
}
int (*point_cmp_pass)(const void *, const void *) = (int (*)(const void *, const void *))point_cmp;

void
point_update(void **root, int x, int y, int *times) {
  point_t **point, *key;

  key = point_key(x, y);
  point = tsearch(key, root, point_cmp_pass);
  if (point == NULL) {
    perror("FATAL: Tree error");
    exit(1);
  }
  if (key != *point) {
    free(key);
    (*point)->times++;
    if ((*point)->times == 2)
      (*times)++;
  }
}

int
main(int argc, char *argv[]) {
  FILE *f;
  int line = 0, times = 0;
  void *root = NULL;

  if (!(f = fopen(argv[1], "r"))) {
    perror("FATAL: Failed to open input file");
    exit(1);
  }
  while (1) {
    int x1, y1, x2, y2;
    int items = fscanf(f, "%d,%d -> %d,%d\n", &x1, &y1, &x2, &y2);
    line++;
    if (items == EOF) {
      break;
    }
    if (items != 4) {
      fprintf(stderr, "FATAL(line %d", line);
      perror("): parse error on line");
    }
    if (y1 == y2) {
      int step = x1 < x2? 1: -1;
      int end_x = x2 + step;
      for (int x = x1; x != end_x; x += step) {
        point_update(&root, x, y1, &times);
      }
    } else if (x1 == x2) {
      int step = y1 < y2? 1: -1;
      int end_y = y2 + step;
      for (int y = y1; y != end_y; y += step) {
        point_update(&root, x1, y, &times);
      }
    } else {
      int step_x = x1 < x2? 1: -1, step_y = y1 < y2? 1: -1;
      int end_x = x2 + step_x, end_y = y2 + step_y;
      int x = x1, y = y1;
      for (; x != end_x; x += step_x, y += step_y) {
        point_update(&root, x, y, &times);
      }
      assert(y == end_y);
    }

  }
  fclose(f);

  printf("%d\n", times);
  exit(0);
}
