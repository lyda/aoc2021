# Day 5 - Hydrothermal Venture

## Problem - part 1

Look for horizontal and vertical line crossings.

  * [Problem](https://adventofcode.com/2021/day/5)
  * Source for input file:
    * `input/input` - `https://adventofcode.com/2021/day/5/input`

## Solution - part 1

  * Easy solve in python: `python/part1.py`
  * Redid it later in C: `c/main1.c`

Added a build step to CI.

## Problem - part 2

...also diagonal.

  * Original problem: https://adventofcode.com/2021/day/5#part2
  * Source for input file (same data):
    * `input/input` - `https://adventofcode.com/2021/day/5/input`

## Solution - part 2

  * Easy in python - once I fixed an off-by-one error: `python/part2.py`
  * Then did a C version: `c/main2.c`
