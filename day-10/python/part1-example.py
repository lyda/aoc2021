#! /usr/bin/env python3

import fileinput

class SyntaxError(Exception):
  pass

def main():
  score = 0
  state = dict()
  closers = {
      '(': ')',
      '[': ']',
      '{': '}',
      '<': '>'
      }
  scores = {
      ')': 3,
      ']': 57,
      '}': 1197,
      '>': 25137
      }
  for line in fileinput.input():
    line = line.rstrip()
    valid_closer = []
    state = dict()
    try:
      for symbol in line:
        if symbol in '([{<':
          closer = closers[symbol]
          state[closer] = state.get(closer, 0) + 1
          valid_closer.append(closer)
        else:
          if not state.get(closer, 0):
            score += scores[symbol]
            raise SyntaxError('Too many ' + symbol)
          closer = valid_closer.pop()
          if not valid_closer or closer != symbol:
            raise SyntaxError(symbol + ' should be: ' + closer)
          state[closer] -= 1
    except SyntaxError:
      pass

  print(score)

if __name__ == '__main__':
  main()
