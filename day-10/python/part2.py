#! /usr/bin/env python3

import fileinput
import statistics

class SyntaxError(Exception):
  pass

def main():
  line_scores = []
  closers = {
      '(': ')',
      '[': ']',
      '{': '}',
      '<': '>'
      }
  scores = {
      ')': 1,
      ']': 2,
      '}': 3,
      '>': 4
      }
  uncorrupted = []
  for line in fileinput.input():
    line = line.rstrip()
    valid_closer = []
    try:
      for symbol in line:
        if symbol in '([{<':
          closer = closers[symbol]
          valid_closer.append(closer)
        else:
          closer = 'None'
          if valid_closer:
            closer = valid_closer[-1]
          if not valid_closer or closer != symbol:
            raise SyntaxError(symbol + ' should be: ' + closer)
          valid_closer.pop()
      score = 0
      valid_closer.reverse()
      for closer in valid_closer:
        score *= 5
        score += scores[closer]
      line_scores.append(score)
    except SyntaxError as err:
      pass

  print(statistics.median(line_scores))

if __name__ == '__main__':
  main()
