#! /usr/bin/env python3

import fileinput

class SyntaxError(Exception):
  pass

def main():
  score = 0
  closers = {
      '(': ')',
      '[': ']',
      '{': '}',
      '<': '>'
      }
  scores = {
      ')': 3,
      ']': 57,
      '}': 1197,
      '>': 25137
      }
  for line in fileinput.input():
    line = line.rstrip()
    valid_closer = []
    try:
      for symbol in line:
        if symbol in '([{<':
          closer = closers[symbol]
          valid_closer.append(closer)
        else:
          closer = 'None'
          if valid_closer:
            closer = valid_closer[-1]
          if not valid_closer or closer != symbol:
            score += scores[symbol]
            raise SyntaxError(symbol + ' should be: ' + closer)
          valid_closer.pop()
    except SyntaxError as err:
      pass

  print(score)

if __name__ == '__main__':
  main()
