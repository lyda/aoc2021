#! /usr/bin/env python3

import fileinput
import math

class Basins:
  def __init__(self, heightmap):
    self.heightmap = heightmap
    self.max_y = len(heightmap) + 1
    self.seen_list = dict()

  def saw(self, y, x):
    self.seen_list[y + (x * self.max_y)] = True

  def seen(self, y, x):
    return self.seen_list.get(y + (x * self.max_y), False)

  def map_basin(self, y, x):
    if self.heightmap[y][x] == 9 or self.seen(y, x):
      return 0
    self.saw(y, x)
    basin_size = 1
    basin_size += self.map_basin(y, x+1)
    basin_size += self.map_basin(y+1, x)
    basin_size += self.map_basin(y, x-1)
    basin_size += self.map_basin(y-1, x)
    return basin_size

def main():
  heightmap = []
  for line in fileinput.input():
    line = line.rstrip()
    if not heightmap:
      heightmap.append([9 for _ in range(len(line) + 2)])
    row = [9]
    row.extend([int(i) for i in line])
    row.append(9)
    heightmap.append(row)
  heightmap.append([9 for _ in range(len(heightmap[0]))])
  basins = Basins(heightmap)
  basin_sizes = []

  for y in range(1, len(heightmap) - 1):
    for x in range(1, len(heightmap[y]) - 1):
      basin_size = basins.map_basin(y, x)
      if basin_size:
        basin_sizes.append(basin_size)

  basin_sizes.sort()
  print(math.prod(basin_sizes[-3:]))

if __name__ == '__main__':
  main()
