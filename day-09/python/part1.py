#! /usr/bin/env python3

import fileinput

def main():
  risk_level = 0
  heightmap = []
  for line in fileinput.input():
    line = line.rstrip()
    if not heightmap:
      heightmap.append([9 for _ in range(len(line) + 2)])
    row = [9]
    row.extend([int(i) for i in line])
    row.append(9)
    heightmap.append(row)
  heightmap.append([9 for _ in range(len(heightmap[0]))])

  for y in range(1, len(heightmap) - 1):
    for x in range(1, len(heightmap[y]) - 1):
      if heightmap[y][x] >= heightmap[y][x+1]:
        continue
      if heightmap[y][x] >= heightmap[y+1][x]:
        continue
      if heightmap[y][x] >= heightmap[y][x-1]:
        continue
      if heightmap[y][x] >= heightmap[y-1][x]:
        continue
      risk_level += 1 + heightmap[y][x]
  print(risk_level)

if __name__ == '__main__':
  main()
