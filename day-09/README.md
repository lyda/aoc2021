# Day 9 - Smoke Basin

## Problem - part 1

Find the low points.

  * [Problem](https://adventofcode.com/2021/day/9)
  * Source for input file:
    * `input/input` - `https://adventofcode.com/2021/day/9/input`

## Solution - part 1

  * python: `python/part-1.py`

## Problem - part 2

This is only visible once the previous one is solved.

  * Original problem: https://adventofcode.com/2021/day/9#part2
  * Source for input file (same data):
    * `input/input` - `https://adventofcode.com/2021/day/9/input`

## Solution - part 2

  * python: `python/part-2.py`
