#! /usr/bin/env python3

import collections
import fileinput

class Enhancer:
  def __init__(self):
    self.image_map = {'.': 0, '#': 1}
    self.image_rmap = ['.', '#']
    self.enhancer = None
    # Note: this breaks the example.  But the map for 0 and 511 reverses
    # the pixels in the input.  There should be code to detect this.
    self.default = False
    self.image = collections.defaultdict(lambda: 0)
    self.row = 0

  def build(self, line):
    for col, pixel in enumerate([self.image_map[c] for c in line]):
      if pixel:
        self.image[tuple([self.row, col])] = 1
    self.row += 1

  def enhance(self):
    self.default = not self.default
    image = collections.defaultdict(lambda: 0)
    if self.default:
      image = collections.defaultdict(lambda: 1)
    min_row = min([coord[0] for coord in self.image.keys()]) - 2
    min_col = min([coord[1] for coord in self.image.keys()]) - 2
    max_row = max([coord[0] for coord in self.image.keys()]) + 3
    max_col = max([coord[1] for coord in self.image.keys()]) + 3
    for row in range(min_row, max_row):
      for col in range(min_col, max_col):
        lookup = 0
        exponent = 8
        for dr in range(-1, 2):
          for dc in range(-1, 2):
            if self.image[tuple([row+dr, col+dc])]:
              lookup += 2 ** exponent
            exponent -= 1
        if self.enhancer[lookup] != int(self.default):
          image[tuple([row, col])] = self.enhancer[lookup]
    self.image = image

  def print(self):
    min_row = min([coord[0] for coord in self.image.keys()]) - 2
    min_col = min([coord[1] for coord in self.image.keys()]) - 2
    max_row = max([coord[0] for coord in self.image.keys()]) + 3
    max_col = max([coord[1] for coord in self.image.keys()]) + 3
    image = self.image.copy()
    for row in range(min_row, max_row):
      output = []
      for col in range(min_col, max_col):
        output.append(self.image_rmap[image[tuple([row, col])]])
      print(''.join(output))
    print('')


def main():
  enhancer = Enhancer()
  for line in fileinput.input():
    line = line.rstrip()
    if not enhancer.enhancer:
      enhancer.enhancer = [enhancer.image_map[c] for c in line]
    elif line:
      enhancer.build(line)

  enhancer.enhance()
  enhancer.enhance()

  print(len(enhancer.image))

if __name__ == '__main__':
  main()
