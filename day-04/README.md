# Day 4 - Giant Squid

## Problem - part 1

Play bingo with a squid!

  * [Problem](https://adventofcode.com/2021/day/4)
  * Source for input file:
    * `input/input` - `https://adventofcode.com/2021/day/4/input`

TODO: Might be interesting to do this in C.

## Solution - part 1

  * Really easy in python: `python/part1.py`

## Problem - part 2

...but try to lose.

  * Original problem: https://adventofcode.com/2021/day/4#part2
  * Source for input file (same data):
    * `input/input` - `https://adventofcode.com/2021/day/4/input`

## Solution - part 2

  * Had to remember the python idiom for copying a list.
    Otherwise easy: `python/part2.py`
