#! /usr/bin/env python3

import fileinput
import re

class Bingo(Exception):
  pass

class Spot:
  def __init__(self, number):
    self.number = int(number)
    self.marked = False

  def mark(self, number):
    if self.number == number:
      self.marked = True

class Board:
  def __init__(self):
    self.board = []

  def add_line(self, line):
    self.board.append([Spot(spot) for spot in line])

  def mark(self, number):
    for line in self.board:
      for spot in line:
        spot.mark(number)
    for line in self.board:
      if all([spot.marked for spot in line]):
        raise Bingo(self, number)
    for y in range(5):
      if all([spot.marked for spot in [self.board[x][y] for x in range(5)]]):
        raise Bingo(self, number)

  def sum_unmarked(self):
    unmarked = 0
    for line in self.board:
      unmarked += sum([spot.number for spot in line if not spot.marked])
    return unmarked

def main():
  numbers = None
  boards = []
  for line in fileinput.input():
    line = line.rstrip()
    if not numbers:
      numbers = [int(n) for n in line.split(',')]
      continue
    if not line:
      boards.append(Board())
      continue
    boards[-1].add_line(re.split(' +', line.lstrip()))

  for number in numbers:
    for board in boards[:]:
      try:
        board.mark(number)
      except Bingo as bingo:
        board = bingo.args[0]
        if len(boards) == 1:
          print(board.sum_unmarked() * number)
          return
        boards.remove(board)

if __name__ == '__main__':
  main()
