#! /usr/bin/env python3

import fileinput
import re
import sys

get_fold_axis_lambdas = {
    'y': lambda point: point[0],
    'x': lambda point: point[1]
    }
set_fold_axis_lambdas = {
    'x': lambda point, fold_point: (point[0], fold_point - (point[1] - fold_point)),
    'y': lambda point, fold_point: (fold_point - (point[0] - fold_point), point[1])
    }

def pprint(paper):
  max_y = 0
  max_x = 0
  for point in paper.keys():
    max_y = max(max_y, point[0])
    max_x = max(max_x, point[1])
  max_y += 1
  max_x += 1
  ap = [['.' for _ in range(max_x)] for _ in range(max_y)]
  for point in paper.keys():
    ap[point[0]][point[1]] = '#'
  answer = '\n'.join([''.join(row) for row in ap]) + '\n'
  return answer

def main():
  paper = dict()
  instructions = []
  reading_instructions = False
  for line in fileinput.input():
    line = line.rstrip()
    if not line:
      reading_instructions = True
      continue
    if not reading_instructions:
      paper[tuple(reversed([int(n) for n in line.split(',')]))] = 1
    else:
      instructions.append(line)

  for instruction in instructions:
    m = re.match(r'fold along ([xy])=(\d+)', instruction)
    get_point = get_fold_axis_lambdas[m[1]]
    set_point = set_fold_axis_lambdas[m[1]]
    fold_point = int(m[2])
    for point in paper.copy().keys():
      if get_point(point) > fold_point:
        new_point = set_point(point, fold_point)
        times_set = paper[point]
        del paper[point]
        paper[new_point] = paper.get(new_point, 0) + times_set

  answer = pprint(paper)
  if answer == open(sys.argv[1] + '.part-2-answer').read():
    print('ARHZPCUH')
  else:
    print(answer)

if __name__ == '__main__':
  main()
