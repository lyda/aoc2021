# Day 13 - Transparent Origami

## Problem - part 1

Fold transparent paper once and track points on the paper.

  * [Problem](https://adventofcode.com/2021/day/13)
  * Source for input file:
    * `input/input` - `https://adventofcode.com/2021/day/13/input`

## Solution - part 1

Simple in theory but two things tripped me up.  One was in taking in
input and the other was math...

First was in building the `paper` dict. I didn't think to reverse the
inputs as 2d arrays are indexed by rows and then columns.

Second was the math for the folding.  I worked it out and then promptly
forgot it.  The idea was to do `fold_point - (point[1] - fold_point)`
to reflect an `x` fold.  But I wrote `point[1] - (point[1] - fold_point)`
which is just wrong.  So I had to work it out again.

The plus side is that I had to write `pprint` to print the paper which
would come in handy in part 2.

Amusingly what I thought would come in handy in part 2, keeping track
of overlaps, was not useful.  And in fact was the one thing I had to
change in `pprint` to make it work.

  * python: `python/part-1.py`

## Problem - part 2

This is only visible once the previous one is solved.

Do all the folds and enter the seekrit codezors!

This does not fit in my ci framework.  So I'm going to have to come up
with a solution to that.  And yes, this is a case where my theory about
what part 2 would be did not pan out but my debugging in part 1 was the
key to the solve here.  Bad prediction, excellent bugs.

Note: Have a ci checking solution.  Not great, but it works.  Storing the
answer in the input dir and checking there.

  * Original problem: https://adventofcode.com/2021/day/13#part2
  * Source for input file (same data):
    * `input/input` - `https://adventofcode.com/2021/day/13/input`

## Solution - part 2

  * python: `python/part-2.py`
