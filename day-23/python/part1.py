#! /usr/bin/env python3

import fileinput
import re

def main():
  left_room = []
  right_room = []
  rooms = [[], [], [], []]
  for line in fileinput.input():
    line = line.rstrip()
    first = re.match(r'###([A-D])#([A-D])#([A-D])#([A-D])###', line)
    if first:
      for i in range(4):
        rooms[i] = [' ', first[i+1]]
    second = re.match(r'  #([A-D])#([A-D])#([A-D])#([A-D])#', line)
    if second:
      for i in range(4):
        rooms[i][0] = second[i+1]

  print(rooms)

if __name__ == '__main__':
  main()
