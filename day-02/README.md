# Day 2 - Dive!

## Problem - part 1

Follow a course using the commands `forward`, `down`, `up`.

  * [Problem](https://adventofcode.com/2021/day/2)
  * Source for input file:
    * `input/input` - `https://adventofcode.com/2021/day/2/input`

## Solution - part 1

  * Really simple to do in awk: `awk/part-1.awk`

## Problem - part 2

Slightly more complex actions based on the same commands.

  * Original problem: https://adventofcode.com/2021/day/2#part2
  * Source for input file (same data):
    * `input/input` - `https://adventofcode.com/2021/day/2/input`

## Solution - part 2

  * Still really simple in awk: `awk/part-1.awk`
