#!/usr/bin/awk -f
BEGIN {
  depth = 0;
  distance = 0;
  aim = 0
}
END {
  print depth * distance;
}
$1 == "forward" {
  distance += $2
  depth += aim * $2
}
$1 == "up" {
  aim -= $2
}
$1 == "down" {
  aim += $2
}
