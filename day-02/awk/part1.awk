#!/usr/bin/awk -f
BEGIN {
  depth = 0;
  distance = 0;
}
END {
  print depth * distance;
}
$1 == "forward" {
  distance += $2
}
$1 == "up" {
  depth -= $2
}
$1 == "down" {
  depth += $2
}
