#! /usr/bin/env python3

import fileinput
import re

def calc_y(dy, min_y, max_y):
  y = 0
  while True:
    if y < min_y:
      return False
    if y < max_y:
      return True
    y += dy
    dy -= 1

def main():
  for line in fileinput.input():
    line = line.rstrip()
  m = re.match(r'target area: x=([0-9-]+)\.\.([0-9-]+), y=([0-9-]+)\.\.([0-9-]+)', line)
  x = [int(x) for x in [m[1], m[2]]]
  y = [int(x) for x in [m[3], m[4]]]

  #print(x, y)

  #for dy in range(1000):
  #  if calc_y(dy, y[0], y[1]):
  #    print(dy, 'hits')
  answer = abs(y[0]) - 1
  answer = (answer + 1) * (answer / 2)
  print(int(answer))

if __name__ == '__main__':
  main()
