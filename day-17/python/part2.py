#! /usr/bin/env python3

import collections
import math
import fileinput
import re

def calc_y_steps(dy, min_y, max_y):
  y = 0
  step = 0
  steps = []
  hit = True
  while True:
    step += 1
    y += dy
    if y < min_y:
      return hit, steps
    if y <= max_y:
      steps.append(step)
      hit = True
    dy -= 1

def calc_x_steps(dx, min_x, max_x, max_steps):
  x = 0
  step = 0
  steps = []
  hit = False
  while step < max_steps:
    step += 1
    x += dx
    #print(dx, step, x, min_x, max_x)
    if x > max_x:
      return hit, steps
    if x >= min_x:
      steps.append(step)
      hit = True
    if dx:
      dx -= 1
  return hit, steps

def main():
  for line in fileinput.input():
    line = line.rstrip()
  m = re.match(r'target area: x=([0-9-]+)\.\.([0-9-]+), y=([0-9-]+)\.\.([0-9-]+)', line)
  x = [int(x) for x in [m[1], m[2]]]
  y = [int(x) for x in [m[3], m[4]]]

  ysteps = collections.defaultdict(list)
  xsteps = collections.defaultdict(list)

  max_dy = abs(y[0]) - 1
  for dy in range(y[0], max_dy + 1):
    hit, steps = calc_y_steps(dy, y[0], y[1])
    if hit:
      for step in steps:
        ysteps[step].append(dy)

  min_dx = abs(int((-1-math.sqrt(1+8*x[0]))/2)) - 1
  max_steps = max(ysteps.keys())
  for dx in range(min_dx, x[1] + 1):
    hit, steps = calc_x_steps(dx, x[0], x[1], max_steps)
    if hit:
      for step in steps:
        xsteps[step].append(dx)

  answer = dict()
  for step in ysteps.keys():
    for dy in ysteps[step]:
      for dx in xsteps[step]:
        answer[tuple([dx, dy])] = 1
  print(len(answer))

if __name__ == '__main__':
  main()
