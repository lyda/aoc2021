#! /usr/bin/env python3

import fileinput
import re

def main():
  occurances = 0
  for line in fileinput.input():
    line = line.rstrip()
    occurances += len([s for s in re.sub(r'.* \| ', '', line).split(' ') if len(s) in [2, 3, 4, 7]])
  print(occurances)

if __name__ == '__main__':
  main()
