#! /usr/bin/env python3

import fileinput
import re

def filter_digits(length, patterns, filterfunc=lambda pat: True):
  return [pat for pat in patterns if len(pat) == length if filterfunc(pat)][0]

def main():
  output_sum = 0
  for line in fileinput.input():
    line = line.rstrip()
    patterns, output = [[''.join(sorted(d)) for d in side.split(' ')] for side in line.split(' | ')]
    digits = ['' for _ in range(10)]
    for digit, length in {1: 2, 4: 4, 7: 3, 8: 7}.items():
      digits[digit] = filter_digits(length, patterns)
    digits[3] = filter_digits(5, patterns,
                              lambda pat: all(c in pat for c in digits[1]))
    digits[9] = filter_digits(6, patterns,
                              lambda pat: all(c in pat for c in digits[4]))
    digits[0] = filter_digits(6, patterns,
                              lambda pat: pat != digits[9] and all([c in pat for c in digits[1]])
                             )
    # Only 6 wire number remaining.
    digits[6] = filter_digits(6, patterns,
                              lambda pat: pat not in [digits[0], digits[9]]
                             )
    digits[5] = filter_digits(5, patterns,
                              lambda pat: all(c in digits[6] for c in pat))
    # Only 5 wire number remaining.
    digits[2] = filter_digits(5, patterns,
                              lambda pat: pat not in [digits[3], digits[5]]
                             )
    digits = dict(zip(digits, range(10)))
    output_sum +=int(''.join([str(digits[pat]) for pat in output]))
  print(output_sum)

if __name__ == '__main__':
  main()
