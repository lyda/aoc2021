#! /usr/bin/env python3

import fileinput
import re

def filter_digits(length, patterns, filterfunc=lambda pat: True):
  return [pat for pat in patterns if len(pat) == length if filterfunc(pat)][0]

def main():
  output_sum = 0
  for line in fileinput.input():
    line = line.rstrip()
    patterns, output = [[''.join(sorted(d)) for d in side.split(' ')] for side in line.split(' | ')]
    digits = ['' for _ in range(10)]
    for digit, length in {1: 2, 4: 4, 7: 3, 8: 7}.items():
      digits[digit] = filter_digits(length, patterns)
    # At this point there are three numbers that use five wires and
    # three that use six.
    # 5: (2, 3, 5) and 6: (0, 6, 9).  I started with the 5 wire ones,
    # but it soon was clear that the 6 wire ones were easier.
    # Found: 1, 4, 7, 8
    # 3 is the only 5 wire digit with both of 1's wires.
    digits[3] = filter_digits(5, patterns,
                              lambda pat: all(c in pat for c in digits[1]))
    # Found: 1, 3, 4, 7, 8
    # Couldn't find an in with any number until 9 - you can find a 4 in a 9.
    digits[9] = filter_digits(6, patterns,
                              lambda pat: all(c in pat for c in digits[4]))
    # Found: 1, 3, 4, 7, 8, 9
    # Now that I knew 9, I could use 1 to find 0 in the 6 wire numbers.
    digits[0] = filter_digits(6, patterns,
                              lambda pat: pat != digits[9] and all([c in pat for c in digits[1]])
                             )
    # Found: 0, 1, 3, 4, 7, 8, 9
    # There's only one 6 wire number remaining: 6
    digits[6] = filter_digits(6, patterns,
                              lambda pat: pat not in [digits[0], digits[9]]
                             )
    # Found: 0, 1, 3, 4, 6, 7, 8, 9
    # This was another frustrating one but doing it backwards you can
    # find a 5 in a 6.
    digits[5] = filter_digits(5, patterns,
                              lambda pat: all(c in digits[6] for c in pat))
    # Found: 0, 1, 3, 4, 5, 6, 7, 8, 9
    # There's only one 5 wire number remaining: 2
    digits[2] = filter_digits(5, patterns,
                              lambda pat: pat not in [digits[3], digits[5]]
                             )
    digits = dict(zip(digits, range(10)))
    output_sum +=int(''.join([str(digits[pat]) for pat in output]))
  print(output_sum)

if __name__ == '__main__':
  main()
