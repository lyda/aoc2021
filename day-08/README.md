# Day 8 - Seven Segment Search

## Problem - part 1

Find all the numbers with a unique number of wires.

  * [Problem](https://adventofcode.com/2021/day/8)
  * Source for input file:
    * `input/input` - `https://adventofcode.com/2021/day/8/input`

## Solution - part 1

  * python: `python/part-1-example.py`
  * python: `python/part-1.py`

## Problem - part 2

Find the rest of the numbers by doing a logic puzzle.

  * Original problem: https://adventofcode.com/2021/day/8#part2
  * Source for input file (same data):
    * `input/input` - `https://adventofcode.com/2021/day/8/input`

## Solution - part 2

  * python: `python/part-2-example.py`
  * This was annoying.  I thought five wire numbers would be easier than
    6 wire ones and the opposite was true.  The hardest two were 9 and 5.
    But 9 was hard because I tried everything else first.  Digit 5 was
    just hard because it took a while to reverse my search.  I marked up
    the python here with my reasoning for each digit: `python/part-2.py`
