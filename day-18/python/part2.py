#! /usr/bin/env python3

import fileinput
import itertools
import json
import math
import regex

class SailFishNumber:
  def __init__(self, number):
    self.number = number

  def tokenize(self):
    return regex.match(r'([^\d]|\d+)+', self.number).captures(1)

  def split(self):
    splited = False
    sf_nums = []
    for token in self.tokenize():
      sf_nums.append(token)
      if splited:
        continue
      if token.isdigit() and int(token) >= 10:
        num = int(token)
        sf_nums.pop()
        sf_nums.append('[')
        sf_nums.append(str(math.floor(num / 2)))
        sf_nums.append(',')
        sf_nums.append(str(math.ceil(num / 2)))
        sf_nums.append(']')
        splited = True
    self.number = ''.join(sf_nums)
    return splited

  def explode(self):
    depth = 0
    numbers = []
    sf_nums = []
    tokens = 0
    state = None
    exploded = False
    for token in self.tokenize():
      sf_nums.append(token)
      if exploded:
        continue
      tokens += 1
      if token == '[':
        depth += 1
        if state == 'left found':
          state = None
      elif token == ',':
        pass
      elif token == ']':
        depth -= 1
      else:  # It's a number.
        numbers.append(token)
        if depth == 5 and state != 'right found':
          if not state:
            state = 'left found'
          elif state == 'left found':
            state = 'right found'
        elif state == 'right found':
          exploded = True
          state = None
          backtrack = []
          for _ in range(tokens):
            backtrack.append(sf_nums.pop())
          if len(numbers) > 3:  # There's a number to the left.
            sf_nums.append(str(int(backtrack.pop()) + int(numbers[-3])))
          while not backtrack[-1].isdigit():
            sf_nums.append(backtrack.pop())
          sf_nums.pop()   # Get rid of the '['
          sf_nums.append('0')
          backtrack.pop()  # Get rid of the left exploded num
          backtrack.pop()  # Get rid of the ','
          backtrack.pop()  # Get rid of the right exploded num
          backtrack.pop()  # Get rid of the ']'
          while not backtrack[-1].isdigit():
            sf_nums.append(backtrack.pop())
          sf_nums.append(str(int(backtrack.pop()) + int(numbers[-2])))
        else:  # Start counting at the number to the left.
          tokens = 1
    if state == 'right found':  # No post right number.
      exploded = True
      backtrack = []
      for _ in range(tokens):
        backtrack.append(sf_nums.pop())
      if len(numbers) >= 3:  # There's a number to the left.
        sf_nums.append(str(int(backtrack.pop()) + int(numbers[-2])))
      while not backtrack[-1].isdigit():
        sf_nums.append(backtrack.pop())
      sf_nums.pop()   # Get rid of the '['
      sf_nums.append('0')
      backtrack.pop()  # Get rid of the left exploded num
      backtrack.pop()  # Get rid of the ','
      backtrack.pop()  # Get rid of the right exploded num
      backtrack.pop()  # Get rid of the ']'
      while backtrack:
        sf_nums.append(backtrack.pop())
    self.number = ''.join(sf_nums)
    return exploded

  def add(self, number):
    self.number = f'[{self.number},{number}]'
    try_reduce = True
    while try_reduce:
      if not self.explode():
        if not self.split():
          try_reduce = False

  def magnitude(self, sf_nums=None):
    if sf_nums is None:
      sf_nums = json.loads(self.number)
    left = sf_nums[0]
    right = sf_nums[1]
    if isinstance(left, int):
      left *= 3
    else:
      left = 3 * self.magnitude(left)
    if isinstance(right, int):
      right *= 2
    else:
      right = 2 * self.magnitude(right)
    return left + right

def main():
  magnitudes = []
  numbers = []
  for line in fileinput.input():
    numbers.append(line.rstrip())
  for pair in itertools.permutations(numbers, 2):
    sftry = SailFishNumber(pair[0])
    sftry.add(pair[1])
    magnitudes.append(sftry.magnitude())

  print(max(magnitudes))

if __name__ == '__main__':
  main()
