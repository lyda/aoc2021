# AOC 2021

These are my solutions for 2021's [Advent of Code]. I might not end up
doing them on the day due to various time commitments, but I'll try.
Chances are I'll do each one in the language that seems the most obvious
for solving the problem.  With the constraints being that it would need
to be one I vaguely know and that's not a huge pain to set up.

I will commit my mistakes.  I think it's useful to know even older coders
still make dumb mistakes.  This is not a field where perfection is a goal.
Better, more thoughtful, feedback - those are goals.  Get comfortable
with them.

I might revisit some days and do them in other languages just to compare
and contrast solutions.  I might also set up a CI/CD system, though I'm
not seeing an API for AOC so that might make it a bit hard.

## Language notes

Notes for various languages.

### Python

The [rich] module is useful for debugging map/graph type problems.

[Advent of Code]: https://adventofcode.com/
[rich]: https://rich.readthedocs.io/en/stable/markup.html
