#! /usr/bin/env python3

import collections
import fileinput
import rich

class Cavern:
  def __init__(self):
    self.cavern = collections.defaultdict(int)
    self.inner_y = 0
    self.max_y = 0
    self.inner_x = 0
    self.max_x = 0

  def append(self, row):
    if not self.inner_x:
      self.inner_x = len(row)
    for dy in range(5):
      for dx in range(5):
        self.cavern.update({tuple([self.inner_y + (dy * self.inner_x),
                                   x + (dx * self.inner_x)]):
                            (risk + dy + dx - 1) % 9 + 1
                            for (x, risk) in enumerate(row)})
    self.inner_y += 1
    self.max_y += 5
    if not self.max_x:
      self.max_x = len(row) * 5

  def get_neighbors(self, spot):
    neighbors = dict()
    for dy in range(-1, 2):
      for dx in range(-1, 2):
        if abs(dx) == abs(dy):
          continue
        ny = spot[0] + dy
        nx = spot[1] + dx
        if ny < 0 or ny >= self.max_y or nx < 0 or nx >= self.max_x:
          continue
        neighbors[tuple([ny, nx])] = self.cavern[tuple([ny, nx])]
    return neighbors

  # https://en.wikipedia.org/wiki/A*_search_algorithm
  def a_star(self):
    start = tuple([0, 0])
    stop = tuple([self.max_y - 1, self.max_x - 1])

    open_list = set([start]) # visited, but have some unchecked neighbours.
    closed_list = set([])    # visited nodes with fully checked neighbours.

    # The f() in A*
    costs = dict()
    costs[start] = 0

    # The g() in A*
    parents = dict()
    parents[start] = start

    while len(open_list) > 0:
      spot = None

      try:
        spot = next(iter(open_list))
      except StopIteration:
        return []

      for open_spot in open_list:
        if costs[open_spot] + self.cavern[open_spot] \
               < costs[spot] + self.cavern[spot]:
          spot = open_spot

      # If the current spot is the stop then we start again from start.
      if spot == stop:
        solution = []
        while parents[spot] != spot:
          solution.append(spot)
          spot = parents[spot]
        solution.append(start)
        solution.reverse()

        #self.pprint(solution)
        return solution

      for (neighbour, risk) in self.get_neighbors(spot).items():
        # If this spot hasn't been seen add, mark it open and put it in
        # costs and parents.
        if neighbour not in open_list and neighbour not in closed_list:
          open_list.add(neighbour)
          parents[neighbour] = spot
          costs[neighbour] = costs[spot] + risk

        elif costs[neighbour] > costs[spot] + risk:
          # Is spot -> neighbour faster?  If it is, update costs and
          # parents and reopen spot if it was closed.
          costs[neighbour] = costs[spot] + risk
          parents[neighbour] = spot
          if neighbour in closed_list:
            closed_list.remove(neighbour)
            open_list.add(neighbour)

      # Neighbours checked.
      open_list.remove(spot)
      closed_list.add(spot)

    return []

  def pprint(self, solution):
    cavern = self.cavern.copy()
    for s in solution:
      cavern[s] = '[bold red]' + str(cavern[s]) + '[/bold red]'
    for y in range(self.max_y):
      rich.print(''.join([str(cavern[tuple([y, x])]) for x in range(self.max_x)]))

def main():
  score = 0
  cavern = Cavern()
  for line in fileinput.input():
    cavern.append([int(risk) for risk in line.rstrip()])
  least_path = cavern.a_star()
  score = sum([cavern.cavern[spot]
               for spot in least_path
               if spot != tuple([0, 0])])

  print(score)

if __name__ == '__main__':
  main()
