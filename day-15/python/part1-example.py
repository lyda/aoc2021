#! /usr/bin/env python3

import collections
import fileinput

class Cavern:
  def __init__(self):
    self.cavern = []
    self.lowest_risk = 0
    self.max_y = 0
    self.max_x = 0

  def append(self, row):
    self.cavern.append(row)

  def finalise_map(self):
    self.max_y = len(self.cavern) - 1
    self.max_x = len(self.cavern[0]) - 1
    self.lowest_risk = (self.max_y * 9) + (self.max_x * 9)

  def least_risky_path(self, y, x, score=0,
                       visited=collections.defaultdict(bool)):
    visited[tuple([y, x])] = True
    if y or x:
      score += self.cavern[y][x]
    if y == self.max_y and x == self.max_x:
      if score < self.lowest_risk:
        self.lowest_risk = score
      return self.lowest_risk
    if self.lowest_risk < score + self.max_y - y + self.max_x - x - 2:
      return self.lowest_risk
    coords = dict()
    for dy in range(0, 2):
      for dx in range(0, 2):
        if abs(dx) == abs(dy):  # Not middle or diagonals.
          continue
        ny = y + dy
        nx = x + dx
        if ny < 0 or ny > self.max_y or nx < 0 or nx > self.max_x:
          continue
        if visited[tuple([ny, nx])]:
          continue
        coords[tuple([ny, nx])] = self.cavern[ny][nx]
    for coord in sorted(coords, key=coords.get):
      self.least_risky_path(coord[0], coord[1], score, visited)
      visited[coord] = False
    return self.lowest_risk



def main():
  score = 0
  cavern = Cavern()
  for line in fileinput.input():
    cavern.append([int(risk) for risk in line.rstrip()])
  cavern.finalise_map()
  score = cavern.least_risky_path(0, 0)

  print(score)

if __name__ == '__main__':
  main()
