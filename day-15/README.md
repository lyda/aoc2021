# Day 15 - Chiton

## Problem - part 1

Find the least risky path.  Did a naive recursive approach and didn't
get the answer.  So got to read up on graph traversal algoritms and
implemented the `A*` algorithm.

  * [Problem](https://adventofcode.com/2021/day/15)
  * Source for input file:
    * `input/input` - `https://adventofcode.com/2021/day/15/input`

## Solution - part 1

  * python: `python/part-1.py`

## Problem - part 2

This is only visible once the previous one is solved.

Same as part 1, but need to make the map even bigger.  Some fun mod
math here.  The `A*` algorithm struggled here so might revisit to see
if there are better options.

  * Original problem: https://adventofcode.com/2021/day/15#part2
  * Source for input file (same data):
    * `input/input` - `https://adventofcode.com/2021/day/15/input`

## Solution - part 2

  * python: `python/part-2.py`
