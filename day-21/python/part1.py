#! /usr/bin/env python3

import fileinput
import re
import sys

class Dice:
  def __init__(self):
    self.value = 1
    self.rolls = 0

  def roll(self):
    value = self.value
    self.value += 1
    if self.value > 100:
      self.value = 1
    self.rolls += 1
    return value

class PlayerWon(Exception):
  pass

class Player:
  def __init__(self, name, position):
    self.name = name
    self.position = position
    self.score = 0

  def move(self, roll):
    self.position += roll
    self.position %= 10
    if not self.position:
      self.position = 10
    self.score += self.position
    if self.score >= 1000:
      raise PlayerWon(self)

def main():
  players = []
  for line in fileinput.input():
    line = line.rstrip()
    m = re.match(r'(.+) starting position: (\d+)', line)
    players.append(Player(m[1], int(m[2])))

  die = Dice()
  while True:
    try:
      for player in players:
        player.move(sum([die.roll() for _ in range(3)]))
    except PlayerWon:
      print(die.rolls * sum([player.score for player in players if player.score < 1000]))
      sys.exit(0)

if __name__ == '__main__':
  main()
