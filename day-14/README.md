# Day 14 - Extended Polymerization

## Problem - part 1

Break a string into tuples and then insert an additional char between
them.  Repeat ten times; subtract the occurrences of the most occurring
char from the least.

  * [Problem](https://adventofcode.com/2021/day/14)
  * Source for input file:
    * `input/input` - `https://adventofcode.com/2021/day/14/input`

## Solution - part 1

  * python: `python/part-1.py`

## Problem - part 2

Same, but do it forty times.

The naive solution I used in part 1 doesn't work.  The key here is to
think about what I need and what I *don't* need.

What I don't need is the chain itself.  I need the stats on the elements.
To do that, I need stats on the tuples.

In each step I have a list of tuples and how often they occur.  From that,
I can come up with a list of new tuples and how often they occur.

For the element stats, I first need stats on the elements in the initial
template.  And then I need to record how many insertions there are.
From that I can find the min and max occurrences.

  * Original problem: https://adventofcode.com/2021/day/14#part2
  * Source for input file (same data):
    * `input/input` - `https://adventofcode.com/2021/day/14/input`

## Solution - part 2

  * python: `python/part-2.py`
