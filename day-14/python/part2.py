#! /usr/bin/env python3

import fileinput
import collections

def return_pairs(template):
  pairs = []
  for i in range(len(template) - 1):
    pairs.append(tuple(template[i:i+2]))
  return pairs

def main():
  rules = dict()
  tstats = collections.defaultdict(int)
  reading_rules = False
  stats = collections.defaultdict(int)
  for line in fileinput.input():
    line = line.rstrip()
    if not line:
      reading_rules = True
      continue
    if not reading_rules:
      # Initial element stats.
      stats = collections.Counter(list(line))
      # Initial pairs.
      tstats = collections.Counter(return_pairs(list(line)))
    else:
      pairs = line.split(' -> ')
      rules[tuple(pairs[0])] = pairs[1]

  for _ in range(40):
    # All the old tuples will be gone and replaced with new ones so
    # create an empty place to put them.
    next_tstats = collections.defaultdict(int)
    for pair in tstats.keys():
      element = rules[pair]
      # Increment stats for this element based on how often this pair exists.
      stats[element] += tstats[pair]
      # Generate new list of tuple occurances.
      for new_pair in [tuple([pair[0], element]), tuple([element, pair[1]])]:
        next_tstats[new_pair] += tstats[pair]
    tstats = next_tstats

  print(max(stats.values()) - min(stats.values()))

if __name__ == '__main__':
  main()
