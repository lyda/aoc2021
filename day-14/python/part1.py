#! /usr/bin/env python3

import fileinput
import collections

def return_pairs(template):
  pairs = []
  for i in range(len(template) - 1):
    pairs.append(tuple(template[i:i+2]))
  return pairs

def main():
  rules = dict()
  template = []
  reading_rules = False
  for line in fileinput.input():
    line = line.rstrip()
    if not line:
      reading_rules = True
      continue
    if not reading_rules:
      template = list(line)
    else:
      pairs = line.split(' -> ')
      rules[tuple(pairs[0])] = pairs[1]

  for _ in range(10):
    pairs = return_pairs(template)
    template = template[0:1]
    for pair in pairs:
      template.append(rules[pair])
      template.append(pair[1])

  stats = collections.Counter(template)
  print(max(stats.values()) - min(stats.values()))

if __name__ == '__main__':
  main()
