#! /usr/bin/env python3

import fileinput
import collections

class Caves:
  def __init__(self):
    self.vertices = collections.defaultdict(list)

  def add_caves(self, caves):
    self.vertices[caves[0]].append(caves[1])
    self.vertices[caves[1]].append(caves[0])

  def visit_small_caves(self, cave,
                        visited=collections.defaultdict(bool),
                        mypath=''):
    if cave == 'end':
      #print(mypath + cave)
      return 1
    small_caves_found = 0
    if visited[cave] and not visited['DOUBLE']:
      visited['DOUBLE'] = True
      visited[cave] = False
    if not visited[cave]:
      if cave.islower():
        visited[cave] = True
      for visit in self.vertices[cave]:
        if visit != 'start':
          small_caves_found += self.visit_small_caves(
              visit,
              visited.copy(),
              mypath + cave + ','
              )
    return small_caves_found

def main():
  caves = Caves()
  for line in fileinput.input():
    line = line.rstrip()
    caves.add_caves(line.split('-'))

  paths = caves.visit_small_caves('start')
  print(paths)

if __name__ == '__main__':
  main()
