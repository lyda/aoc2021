#! /usr/bin/env python3

import fileinput
import collections

class Caves:
  def __init__(self):
    self.vertices = collections.defaultdict(list)
    self.small_caves = collections.defaultdict(bool)

  def add_caves(self, caves):
    self.small_caves.update({cave: True for cave in caves if cave.islower()})
    self.vertices[caves[0]].append(caves[1])
    self.vertices[caves[1]].append(caves[0])

  def visit_small_caves(self, cave, small_caves=None,
                        visited=collections.defaultdict(bool),
                        mypath=''):
    if not small_caves:
      small_caves = self.small_caves.copy()
    if cave == 'end':
      #print(mypath + cave)
      return 1
    small_caves_found = 0
    if not visited[cave]:
      if cave.islower():
        visited[cave] = True
      if small_caves[cave]:
        del small_caves[cave]
      for visit in self.vertices[cave]:
        if visit != 'start':
          small_caves_found += self.visit_small_caves(
              visit,
              small_caves.copy(),
              visited.copy(),
              mypath + cave + '-'
              )
    return small_caves_found

def main():
  caves = Caves()
  for line in fileinput.input():
    line = line.rstrip()
    caves.add_caves(line.split('-'))

  paths = caves.visit_small_caves('start')
  print(paths)

if __name__ == '__main__':
  main()
