#define _POSIX_C_SOURCE 200809L
#include <assert.h>
#include <ctype.h>
#include <search.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct caves {
  char *cave;
  int index;
  int count;
  char **caves;
} caves_t;

int *
new_visits(int length) {
  static int visits_len = 0;
  
  if (length) {
    visits_len = length;
  }
  assert(visits_len > 0);
  int *visits = (int *)malloc(sizeof(int) * visits_len);
  assert(visits);
  memset(visits, 0, sizeof(int) * visits_len);
  return visits;
}

int
cave_index(char *cave) {
  static char **caves = NULL;
  static int index = 0;

  if (!cave)
    return index;
  if (index)
    assert(caves);
  for (int i = 0; i < index; i++)
    if (!strcmp(cave, caves[i]))
      return i;
  index++;
  if (caves)
    // cppcheck-suppress memleakOnRealloc
    caves = (char **)realloc(caves, sizeof(char *) * index);
  else
    caves = (char **)malloc(sizeof(char *) * index);
  assert(caves);
  caves[index - 1] = cave;
  return index - 1;
}

int
caves_cmp(caves_t *a, caves_t *b) {
  return strcmp(a->cave, b->cave);
}
int (*caves_cmp_pass)(const void *, const void *) = (int (*)(const void *, const void *  ))caves_cmp;

void
add_cave(void **caves, int **small_caves, char cave_names[2][10]) {  /* Flawfinder: ignore */
  for (int i=0, j=1; i < 2; i++, j--) {
    caves_t **needle, *cave;

    cave = (caves_t *)malloc(sizeof(caves_t));
    assert(cave);
    cave->cave = cave_names[i];  /* Flawfinder: ignore */
    needle = tsearch(cave, caves, caves_cmp_pass);
    assert(needle);
    if (cave != *needle) {
      free(cave);
      cave = *needle;
    } else {
      int *tmp;

      cave->cave = strdup(cave->cave);
      cave->index = cave_index(cave->cave);
      cave->count = 0;
      cave->caves = NULL;
      tmp = (int *)malloc(sizeof(int) * cave->index + 1);
      tmp[cave->index] = 0;
      if (*small_caves) {
        memcpy(tmp, *small_caves, sizeof(int) * cave->index);  /* Flawfinder: ignore */
        free(*small_caves);
      }
      *small_caves = tmp;
      if (strlen(cave->cave) == 2 && islower(cave->cave[0])) {  /* Flawfinder: ignore */
        (*small_caves)[cave->index] = 1;
      }
    }
    cave->count++;
    if (cave->caves)
      cave->caves = (char **)realloc(cave->caves, sizeof(char *) * cave->count);
    else 
      cave->caves = (char **)malloc(sizeof(char *) * cave->count);
    assert(cave->caves);
    cave->caves[cave->count - 1] = strdup(cave_names[j]);
  }
}

int
visit_small_caves_once(void **caves, int *small_caves, char *cave_name, int *visits) {
  if (!strcmp(cave_name, "end")) {
    for (int i = 0; i < cave_index(NULL); i++)
      if (small_caves[i] && visits[i] != 1)
        return 0;
    return 1;
  }
  caves_t needle, **found, *cave;
  needle.cave = cave_name;
  found = tfind(&needle, caves, caves_cmp_pass);
  assert(found);
  cave = *found;
  assert(cave);
  if (strlen(cave->cave) == 2 && islower(cave->cave[0])) {  /* Flawfinder: ignore */
    visits[cave->index]++;
    if (visits[cave->index] > 1)
      return 0;
  }
  int paths = 0;
  int *next_visits = new_visits(0);
  assert(next_visits);
  for (int i = 0; i < cave->count; i++) {
    if (strcmp(cave->caves[i], "start")) {
      memcpy(next_visits, visits, sizeof(int) * cave_index(NULL));  /* Flawfinder: ignore */  // NOLINT
      paths += visit_small_caves_once(caves, small_caves, cave->caves[i], next_visits);
    }
  }
  free(next_visits);
  return paths;
}

int
main(int argc, char *argv[]) {
  FILE *f;
  void *caves = NULL;
  int *small_caves = NULL;

  f = fopen(argv[1], "r");  /* Flawfinder: ignore */
  assert(f);
  while (1) {
    char cave_names[2][10];  /* Flawfinder: ignore */
    int scanned = fscanf(f, "%9[^-]-%9s\n",              /* Flawfinder: ignore */  // NOLINT
                         cave_names[0], cave_names[1]);
    if (scanned == EOF)
      break;
    add_cave(&caves, &small_caves, cave_names);
  }
  fclose(f);
  assert(caves);
  /* Sets visits length to the number of caves. */
  int *visits = new_visits(cave_index(NULL));
  printf("%d\n", visit_small_caves_once(&caves, small_caves, "start", visits));
  free(visits);

  exit(0);
}
