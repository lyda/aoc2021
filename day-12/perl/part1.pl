#!/usr/bin/perl

use Modern::Perl;

package Caves;

sub new {
  my $type = shift;
  my $this = {};

  $this->{"vertices"} = {};
  $this->{"small_caves"} = {};
  return bless $this, $type;
}

sub add_caves {
  my $this = shift;
  my @caves = @_;

  push(@{$this->{"vertices"}{$caves[0]}}, $caves[1]);
  push(@{$this->{"vertices"}{$caves[1]}}, $caves[0]);
}

sub visit {
  my $this = shift;
  my $cave = shift;
  my %visited;
  %visited = @_ if (@_);

  return 1 if ($cave eq "end");
  my $small_caves = 0;
  if (!exists($visited{$cave})) {
    $visited{$cave} = 1 if ($cave eq lc $cave);
    foreach my $visit (@{$this->{"vertices"}{$cave}}) {
      $small_caves += $this->visit($visit, %visited)
        if ($visit ne "start");
    }
  }
  return $small_caves;
}

package main;

my $caves = new Caves();
while (<>) {
  chomp;
  $caves->add_caves(split(/-/));
}

my $paths = $caves->visit("start");
print("$paths\n");
