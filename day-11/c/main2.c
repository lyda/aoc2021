#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>

#define BEGIN_BOARD_LOOP(start_y, end_y, start_x, end_x) \
                         for (int y = start_y; y < end_y; y++) { \
                           for (int x = start_x; x < end_x; x++) {
#define END_BOARD_LOOP } }

struct point_list {
  int y;
  int x;
  struct point_list *next;
};

void
flash_point(struct point_list **flashed, int y, int x) {
  struct point_list *next;
  if (!*flashed) {
    *flashed = (struct point_list *)malloc(sizeof(struct point_list));
    next = *flashed;
    next->next = NULL;
  } else {
    next = (struct point_list *)malloc(sizeof(struct point_list));
    next->next = (*flashed)->next;
    (*flashed)->next = next;
  }
  next->y = y;
  next->x = x;
}

int
main(int argc, char *argv[]) {
  FILE *f;
  int octopuses[12][12];

  if (!(f = fopen(argv[1], "r"))) {
    perror("FATAL: Failed to open input file");
    exit(1);
  }
  BEGIN_BOARD_LOOP(0, 12, 0, 12)
    if (x == 0 || x == 11 || y == 0 || y == 11) {
      octopuses[y][x] = -1;
      continue;
    }
    int c = fgetc(f);
    if (c == '\n')
      c = fgetc(f);
    if (c == EOF) {
      fprintf(stderr, "FATAL: Ran out of data at %d, %d.", x, y);
      exit(1);
    }
    octopuses[y][x] = c - '0';
  END_BOARD_LOOP
  fclose(f);

  int steps = 0;
  int flash_count = 0;
  while (flash_count < 100) {
    struct point_list *flashed = NULL;

    flash_count = 0;
    steps++;
    BEGIN_BOARD_LOOP(1, 11, 1, 11)
      if (++octopuses[y][x] == 10)
        flash_point(&flashed, y, x);
    END_BOARD_LOOP
    while (flashed != NULL) {
      BEGIN_BOARD_LOOP(flashed->y-1, flashed->y + 2, flashed->x-1, flashed->x + 2)
        /* not the middle and not the edges of the octopuses */
        if (!y || !x || octopuses[y][x] < 0)
          continue;
        if (++octopuses[y][x] == 10)
          flash_point(&flashed, y, x);
      END_BOARD_LOOP
      struct point_list *doomed = flashed;
      flashed = flashed->next;
      free(doomed);
    }

    BEGIN_BOARD_LOOP(1, 11, 1, 11)
      if (octopuses[y][x] > 9) {
        flash_count++;
        octopuses[y][x] = 0;
      }
    END_BOARD_LOOP

  }


  printf("%d\n", steps);
  exit(0);
}
