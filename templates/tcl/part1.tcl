#!/usr/bin/tclsh

set fp [open $::argc(1) r]
fconfigure $fp -buffering line
gets $fp line
while {$line != ""} {
     gets $fp line
     set line [string trim $line]
}
close $fp
