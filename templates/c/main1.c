#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>

int
main(int argc, char *argv[]) {
  FILE *f;
  uint64_t result = 0;

  if (!(f = fopen(argv[1], "r"))) {
    perror("FATAL: Failed to open input file");
    exit(1);
  }
  /*  Read file here. */
  fclose(f);

  printf("%" PRIu64 "\n", result);
  exit(0);
}
