# Day 16 - Packet Decoder

## Problem - part 1

Parse the packets; sum the version numbers of the packets.

  * [Problem](https://adventofcode.com/2021/day/16)
  * Source for input file:
    * `input/input` - `https://adventofcode.com/2021/day/16/input`

## Solution - part 1

  * python: `python/part-1.py`

## Problem - part 2

This is only visible once the previous one is solved.

Given the operators, parse the values from the message.

  * Original problem: https://adventofcode.com/2021/day/16#part2
  * Source for input file (same data):
    * `input/input` - `https://adventofcode.com/2021/day/16/input`

## Solution - part 2

  * python: `python/part-2.py`
