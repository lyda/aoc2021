#! /usr/bin/env python3

import fileinput

class Parser:

  def __init__(self):
    self.version_total = 0

  def parse(self, packet):
    while packet and '1' in packet:
      #print('parsing packet: "{0}"'.format(packet))
      packet = self.parse_packet(packet)

  def parse_packet(self, packet):
    version = int(packet[0:3], 2)
    self.version_total += version
    ptype = int(packet[3:6], 2)
    #print('version: {0}, type: {1}'.format(version, ptype))
    packet = packet[6:]
    if ptype == 4:
      literal = ''
      while packet[0] == '1':
        literal += packet[1:5]
        packet = packet[5:]
      literal += packet[1:5]
      packet = packet[5:]
      literal = int(literal, 2)
      #print(literal)
    else:
      #print('length type ID: {0}'.format(packet[0]))
      if packet[0] == '0':
        length = int(packet[1:16], 2)
        #print('length: {0}'.format(length))
        packet = packet[16:]
        subpacket = packet[:length]
        #print('begin subpacket parse')
        while subpacket:
          subpacket = self.parse_packet(subpacket)
        #print('end subpacket parse')
        packet = packet[length:]
      else:
        packet_count = int(packet[1:12], 2)
        #print('packet count: {0}'.format(packet_count))
        packet = packet[12:]
        for _ in range(packet_count):
          packet = self.parse_packet(packet)
        #print('end subpacket parse')
    return packet


def main():
  packet = ''
  for line in fileinput.input():
    line = line.rstrip()
    packet += ''.join(['{0:04b}'.format(int(c, 16)) for c in line])

  parser = Parser()
  parser.parse(packet)

  print(parser.version_total)

if __name__ == '__main__':
  main()
