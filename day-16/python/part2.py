#! /usr/bin/env python3

import fileinput
import math

class Parser:

  def __init__(self):
    self.version_total = 0

  def parse(self, packet):
    while packet and '1' in packet:
      packet, literal = self.parse_packet(packet)
    return literal

  def parse_packet(self, packet):
    version = int(packet[0:3], 2)
    self.version_total += version
    ptype = int(packet[3:6], 2)
    packet = packet[6:]
    literal = 0
    if ptype == 4:
      literal = ''
      while packet[0] == '1':
        literal += packet[1:5]
        packet = packet[5:]
      literal += packet[1:5]
      packet = packet[5:]
      literal = int(literal, 2)
    else:
      literals = []
      if packet[0] == '0':
        length = int(packet[1:16], 2)
        packet = packet[16:]
        subpacket = packet[:length]
        while subpacket:
          subpacket, literal = self.parse_packet(subpacket)
          literals.append(literal)
        packet = packet[length:]
      else:
        packet_count = int(packet[1:12], 2)
        packet = packet[12:]
        for _ in range(packet_count):
          packet, literal = self.parse_packet(packet)
          literals.append(literal)
      if ptype == 0:
        literal = sum(literals)
      elif ptype == 1:
        literal = math.prod(literals)
      elif ptype == 2:
        literal = min(literals)
      elif ptype == 3:
        literal = max(literals)
      elif ptype == 5:
        literal = int(literals[0] > literals[1])
      elif ptype == 6:
        literal = int(literals[0] < literals[1])
      elif ptype == 7:
        literal = int(literals[0] == literals[1])
    return packet, literal


def main():
  packet = ''
  for line in fileinput.input():
    line = line.rstrip()
    packet += ''.join(['{0:04b}'.format(int(c, 16)) for c in line])

  parser = Parser()
  print(parser.parse(packet))

if __name__ == '__main__':
  main()
