#! /usr/bin/env python3

import fileinput

def main():
  totals = []
  readings = 0
  for reading in fileinput.input():
    reading = reading.rstrip()
    if not totals:
      totals = [int(digit) for digit in reading]
      continue
    totals = [sum(values) for values in
              zip(totals, [int(digit) for digit in reading])]
    readings += 1

  gamma = ''
  epsilon = ''

  for total in totals:
    gamma += '1' if total > (readings / 2) else '0'
    epsilon += '1' if (readings / 2) > total else '0'

  gamma = int(gamma, 2)
  epsilon = int(epsilon, 2)

  print(gamma * epsilon)

if __name__ == '__main__':
  main()
