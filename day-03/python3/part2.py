#! /usr/bin/env python3

import fileinput

class Readings:
  def __init__(self):
    self.readings = {}

  def save(self, reading):
    readings = self.readings
    for digit in reading:
      digit = int(digit)
      readings[digit] = readings.get(digit, {})
      readings = readings[digit]
      count = readings.get('count', 0)
      readings['count'] = count + 1
      if count == 0:
        readings['reading'] = reading
      elif count == 1:
        del readings['reading']

  def find(self, quantity_func, default_if_equal):
    readings = self.readings
    while True:
      digit = 0
      if readings[0].get('count', 0) == readings[1].get('count', 0):
        digit = default_if_equal
      elif readings[1].get('count', 0) == quantity_func(readings[0].get('count', 0),
                                                      readings[1].get('count', 0)):
        digit = 1
      readings = readings[digit]
      if readings['count'] == 1:
        return readings['reading']
    raise KeyError

def main():
  readings = Readings()

  for reading in fileinput.input():
    reading = reading.rstrip()
    readings.save(reading)

  oxygen = int(readings.find(max, 1), 2)
  co2 = int(readings.find(min, 0), 2)

  print(oxygen * co2)


if __name__ == '__main__':
  main()
