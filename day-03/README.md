# Day 3 - Binary Diagnostic

## Problem - part 1

Parse a binary diagnostic report.

  * [Problem](https://adventofcode.com/2021/day/3)
  * Source for input file:
    * `input/input` - `https://adventofcode.com/2021/day/3/input`

First pass on handling example input this day.

## Solution - part 1

  * This would be a giant pain in awk.  So tried
    perl at first.  `perl/part1.pl`
  * Once I read part 2, I switched to python.  I redid this step in
    python: `python/part1.py`

## Problem - part 2

More binary diagnostic reporting but more complicated.  Doable in perl
but, for me, easier in python.

  * Original problem: https://adventofcode.com/2021/day/3#part2
  * Source for input file (same data):
    * `input/input` - `https://adventofcode.com/2021/day/3/input`

## Solution - part 2

  * This was pretty easy in python but I did stumble due to not reading
    the problem completely.  Once I did that it was pretty easy.  This is
    where I finally got around to handling example input in the ci.
    `python/part2.py`
