#!/usr/bin/perl

use Modern::Perl;

my($readings, @totals);
while (<>) {
  chomp;
  my @reading = split(//);
  for (my $i = 0; $i < scalar(@reading); $i++) {
    $totals[$i] += $reading[$i];
  }
  $readings++;
}

my($gamma, $epsilon);
foreach (@totals) {
  $gamma .= $_ > ($readings / 2)? "1": "0";
  $epsilon .= ($readings / 2) > $_? "1": "0";
}

$gamma = oct("0b" . $gamma);
$epsilon = oct("0b" . $epsilon);

print(($gamma * $epsilon) . "\n");
