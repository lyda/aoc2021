#! /usr/bin/env python3

import fileinput

def main():
  result = 0
  for line in fileinput.input():
    line = line.rstrip()

  print(result)

if __name__ == '__main__':
  main()
