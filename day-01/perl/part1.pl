#!/usr/bin/perl

use Modern::Perl;

my $depth = -1;
my $increased = -1;
while (my $line = <>) {
  chomp $line;
  $line = int($line);
  if ($line > $depth) {
    $increased++;
  }
  $depth = $line;
}
print "$increased\n";
