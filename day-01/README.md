# Day 1 - Sonar Sweep

## Problem - part 1

In a table of numbers, count the increases.

  * [Problem](https://adventofcode.com/2021/day/1)
  * Source for input file:
    * `input/input` - `https://adventofcode.com/2021/day/1/input`

## Solution - part 1

  * Simple to do in awk: `awk/part-1.awk`

## Problem - part 2

This is only visible once the previous one is solved.

  * Original problem: https://adventofcode.com/2021/day/1#part2
  * Source for input file (same data):
    * `input/input` - `https://adventofcode.com/2021/day/1/input`

## Solution - part 2

  * Doable in awk, but arrays trigger alarms in shell/awk. A lack of
    push/pop is disappointing, but fine with awk's associative arrays:
    `awk/part-1.awk`
