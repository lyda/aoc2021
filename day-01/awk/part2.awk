#!/usr/bin/awk -f
BEGIN {
  # Index of the second window.
  i2 = -3;
}
END {
  print increased;
}
{
  depths[i2 + 3] = $1;
  window2 = 0;
  for (i = 0; i < 3; i++) {
    window2 += depths[i2 + i];
  }

  if (i2 >= 0) {
    if (window2 > window1) {
      increased++;
    }
  }
  window1 = window2;
  delete depths[i2 - 1];
  i2++;
}
