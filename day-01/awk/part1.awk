#!/usr/bin/awk -f
BEGIN {
  depth = -1;
  increased = -1;
}
END {
  print increased;
}
{
  if ($1 > depth) {
    increased++;
  }
  depth = $1;
}
