#! /usr/bin/env lua5.3

local i2 = -3
local increased = 0
local depths = {}
depths[-3] = 0
depths[-2] = 0
depths[-1] = 0
local window1 = 0

for line in io.lines(arg[1]) do
  local d = string.gsub(line, "\n", "") + 0
  depths[i2 + 3] = d
  local window2 = 0;
  for i = 0, 2, 1 do
    window2 = window2 + depths[i2 + i];
  end

  if i2 >= 0 then
    if window2 > window1 then
      increased = increased + 1
    end
  end
  window1 = window2;
  depths[i2 - 1] = nil
  i2 = i2 + 1
end

io.write(increased.."\n")
