#! /usr/bin/env lua5.3

local depth = -1
local increased = -1

for line in io.lines(arg[1]) do
  local d = string.gsub(line, "\n", "")
  d = d + 0
  if d > depth then
    increased = increased + 1
  end
  depth = d
end

io.write(increased.."\n")
