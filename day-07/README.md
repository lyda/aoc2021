# Day 7 - The Treachery of Whales

## Problem - part 1

Help crabs move efficiently

  * [Problem](https://adventofcode.com/2021/day/7)
  * Source for input file:
    * `input/input` - `https://adventofcode.com/2021/day/7/input`

## Solution - part 1

  * Easy solve in python - but set a trap for myself.  Initialised the
    `fuel` variable badly: `python/part1.py`
  * Redid it later in Lua: `lua/part1.lua`

## Problem - part 2

Same - but the way to calculate fuel costs changed.  Seemed simple, but
it didn't perform well till I recreated [Gauss's method] for summing
a sequence.  Though I converted n * (n + 1) / 2 to the less nice n *
(n / 2 + 0.5).  The same, but not as nice to look at.

I got the answer just by doing `sum(range(distance + 1))` but
later redid it with the formula.  Way faster.

  * Original problem: https://adventofcode.com/2021/day/7#part2
  * Source for input file (same data):
    * `input/input` - `https://adventofcode.com/2021/day/7/input`

## Solution - part 2

  * Easy in python: `python/part2.py`
  * Then did a lua version: `lua/part2.lua`.  I had completed both
    parts in python before doing the lua one.  So wrote it to handle
    both solutions.
