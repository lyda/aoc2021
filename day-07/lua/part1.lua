#! /usr/bin/env lua5.3

local function reduce(tbl, func, pos)
  local result = 0
  for _, p in ipairs(tbl) do
    result = result + func(pos, p)
  end
  return result
end

local function absdiff(a, b)
  return math.abs(a - b)
end

local input = io.open(arg[1], "r")
if not input then
  return -1
end
local line = input:read("*a")
input:close()
local positions = {}
for position in string.gmatch(line, "([^,\n]+)") do
  table.insert(positions, tonumber(position))
end
local fuel = nil
for position = 0,math.max(table.unpack(positions)) do
  local this_fuel = reduce(positions, absdiff, position)
  if not fuel or this_fuel < fuel then
    fuel = this_fuel
  end
end
io.write(fuel.."\n")
