#! /usr/bin/env python3

import fileinput

def main():
  positions = None
  for line in fileinput.input():
    line = line.rstrip()
    positions = [int(n) for n in line.split(',')]
  fuel = sum(positions)
  for position in range(min(positions), max(positions) + 1):
    fuel_used = sum([abs(position - p) for p in positions])
    if fuel_used < fuel:
      fuel = fuel_used
  print(fuel)

if __name__ == '__main__':
  main()
