#! /usr/bin/env python3

import fileinput

def main():
  positions = None
  for line in fileinput.input():
    line = line.rstrip()
    positions = [int(n) for n in line.split(',')]
  fuel = None
  for pos in range(min(positions), max(positions) + 1):
    fuel_used = sum([int(abs(p - pos) * (abs(p - pos)/2 + 0.5))
                     for p in positions])
    if not fuel or fuel_used < fuel:
      fuel = fuel_used
  print(fuel)

if __name__ == '__main__':
  main()
